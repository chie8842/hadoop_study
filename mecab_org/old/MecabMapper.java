import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import org.chasen.mecab.MeCab;
import org.chasen.mecab.Tagger;
import org.chasen.mecab.Model;
import org.chasen.mecab.Lattice;
import org.chasen.mecab.Node;



public class MecabMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  private static final int MISSING = 9999;
 
  public void map(LongWritable key, Text value,
      OutputCollector<Text, IntWritable> output, Reporter reporter)
      throws IOException {

     try {
       System.loadLibrary("MeCab");
      } catch (UnsatisfiedLinkError e) {
         System.err.println("Cannot load the example native code.\nMake sure your LD_LIBRARY_PATH contains \'.\'\n" + e);
        System.exit(1);
      }
     
      Tagger  tagger = new Tagger();
      String line = value.toString;
      Node node = tagger.parseToNode(str);

      while (node.hasNext()){
	String surface = node.next();
	String feature = node.feature();

	String featureArr[] = feature.split(",");
        if ( featureArr[0].equals("名詞")||featureArr[0].equals("動詞")||featureArr[0].equals("形容詞")){
           context.write(new Text(featureArr[6]), new StringWritable(featureArr[0]));
	}
      }

  }
}
      

