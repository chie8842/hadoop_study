import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MecabReducer extends Reducer<Text, IntWritable, Text, IntWritable> {

  private IntWritable result = new IntWritable();
  @Override
  public void reduce(Text key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {
    
    int sumValue = 0;
    for (IntWritable value : values) {
      sumValue++;
    }
    result.set(sumValue);
    System.out.println(key + "=" + sumValue);
    context.write(key, result);
  }
}
