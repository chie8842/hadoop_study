#!/bin/bash

hadoop jar ~/src/java/mecab_hash/target/mecab-1.0-SNAPSHOT.jar Mecab -libjars /usr/local/bin/mecab-java/MeCab.jar -Dmapred.reduce.slowstart.completed.maps="0.8" -Dmapred.compress.map.output="true" /data/wikipedia/input_128M/*/* /data/wikipedia/output_wiki_128M 3 0 0

hadoop jar ~/src/java/mecab_hash/target/mecab-1.0-SNAPSHOT.jar Mecab -libjars /usr/local/bin/mecab-java/MeCab.jar -Dmapred.reduce.slowstart.completed.maps="0.8" -Dmapred.compress.map.output="true" /data/wikipedia/input2/*/* /data/wikipedia/output_wiki_250K 3 0 0
