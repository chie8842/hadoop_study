import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Mecab extends Configured implements Tool {

  @Override
  public int run(String[] args) throws Exception {

    if (args.length != 5) {
      System.err.println("Usage: Mecab <input path> <output path> <numReduceTasks> <Combiner usage:0 or 1> <Partitioner usage:0 or 1>");
      System.exit(-1);
    }

   Job job =Job.getInstance(getConf(), "Mecab");

//   Configuration conf = job.getConfiguration();
   job.getConfiguration().setBoolean("mapred.used.genericoptionsparser", true);

   job.setJarByClass(Mecab.class);
   job.setJobName("Mecab");

   FileInputFormat.addInputPaths(job, args[0]);
   FileOutputFormat.setOutputPath(job, new Path(args[1]));


   job.setMapperClass(MecabMapper.class);
   job.setReducerClass(MecabReducer.class);
   
   if (Integer.parseInt(args[3]) == 1){
     job.setCombinerClass(MecabReducer.class);
   }

//   job.setMapOutputKeyClass(Text.class);
  // job.setMapOutputValueClass(Text.class);

   job.setOutputKeyClass(Text.class);
   job.setOutputValueClass(IntWritable.class);

   int numRtask = Integer.parseInt(args[2]);
   job.setNumReduceTasks(numRtask);

   if (Integer.parseInt(args[4]) == 1){
     job.setPartitionerClass(MyPartitioner.class);
   }

   return job.waitForCompletion(true) ? 0 : 1;

  }
  
  public static void main(String[] args) throws Exception {
//    System.exit(ToolRunner.run(new Mecab(), args));
     System.exit(ToolRunner.run(new Configuration(), new Mecab(), args));

  }
}
