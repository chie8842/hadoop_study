#!/usr/bin/env python
#coding:utf-8
import os
import sys

# mp3_to_mfcc.py
# usage: python mp3_to_mfcc.py [mp3dir] [mfccdir] [rawdir]
# ディレクトリ内のMP3ファイルからMFCCを抽出する

def mp3ToRaw(mp3File, rawFile):
    # mp3を16kHz, 32bitでリサンプリング
#    print("lame --resample 16 -b 32 -a %s /tmp/music/temp.mp3" % mp3File)
    os.system("lame --resample 16 -b 32 -a %s %s" % (mp3File, mp3File.replace(".mp3","temp.mp3")))
    # mp3をwavに変換
#    print("lame --decode /tmp/music/temp.mp3 /tmp/music/temp.wav")
    os.system("lame --decode %s %s" % (mp3File.replace(".mp3","temp.mp3"),mp3File.replace(".mp3","temp.wav")))
    # wavをrawに変換
#    print("sox /tmp/music/temp.wav %s" % rawFile)
    os.system("/usr/local/bin/sox %s %s" % (mp3File.replace(".mp3","temp.wav"),rawFile))
#    print "endmp3ToRaw"
    os.remove(mp3File.replace(".mp3","temp.mp3"))
    os.remove(mp3File.replace(".mp3","temp.wav"))

def calcNumSample(rawFile):
    # 1サンプルはshort型（2byte）なのでファイルサイズを2で割る
    filesize = os.path.getsize(rawFile)
    numsample = filesize / 2
    return numsample

def extractCenter(inFile, outFile, period):
    # 波形のサンプル数を求める
    numsample = calcNumSample(inFile)

    fs = 16000
    center = numsample / 2
    start = center - fs * period
    end = center + fs * period
    
    # period*2秒未満の場合は範囲を狭める
    if start < 0: start = 0
    if end > numsample - 1: end = numsample - 1

    # SPTKのbcutコマンドで切り出す
    os.system("/usr/local/bin/bcut +s -s %d -e %d < '%s' > '%s'" \
              % (start, end, inFile, outFile))

def calcMFCC(inFile, mfccFile):
    # サンプリング周波数: 16kHz
    # フレーム長: 400サンプル
    # シフト幅  : 160サンプル
    # チャンネル数: 40
    # MFCC: 19次元 + エネルギー
#    os.system("x2x +sf < '%s' | frame -l 400 -p 160 | mfcc -l 400 -f 16 -n 40 -m 19 -E > '%s'"
#              % (rawFile, mfccFile))
#     sys.stderr.write('1')
     os.system("/usr/local/bin/x2x +sf < '%s' | /usr/local/bin/frame -l 400 -p 160 | /usr/local/bin/mfcc -l 400 -f 16 -n 40 -m 19 > '%s'"
              % (inFile, mfccFile))
if __name__ == "__main__":
  for line in sys.stdin:
    inputdir = "/tmp/music"
    inputdata = "%s/%s" % (inputdir,line.rstrip("\n"))
    rawdata = "%s.raw" % (inputdata.replace(".mp3",".raw"))
    rawshortdata = inputdata.replace(".mp3", ".raw")
    mfccdata = inputdata.replace(".mp3", ".mfc")
    outputdata = inputdata.replace(".mp3", ".txt")
    
#    print "%s\t%s" % (mfccdata, outputdata)   
    os.system("mkdir -p %s" % inputdir)
#    sys.stderr.write('%s' % inputdata)
    os.system("hdfs dfs -get /data/music/inputdata/%s %s/" % (line.rstrip("\n"), inputdir))
    sys.stderr.write('3')
    # MP3を変換
    mp3ToRaw(inputdata, rawdata) 
    # 中央の30秒だけ抽出してrawFileへ
    extractCenter(rawdata, rawshortdata, 15)

    # MFCCを計算
    calcMFCC(rawshortdata, mfccdata)

    os.remove(rawdata)

    # convert binary mcp file to text file
    os.system("python print_mfcc.py %s 19 > %s" % (mfccdata, outputdata))

    os.system("hdfs dfs -put %s /data/music/mfc/%s" % (outputdata, line.rstrip("\n").replace(".mp3", ".txt")))
    print "%s\t%s" % (line.rstrip("\n"),1)

