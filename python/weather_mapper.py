#!/usr/bin/env python

import re
import sys

for line in sys.stdin:
  val = line.split(",") 
  date = "/".join([val[1], val[2]])
  rain = val[3]
  tempmax = val[5]
  tempmin = val[6]
  wind = val[7]

  point = 1
 
  # if summary of rain of the day is more than 10, the point is 0.
  if rain and float(rain) > 10:
    point = 0
  # if average of wind of the day is stronger than 7, the point is 0.
  elif wind and wind != "\n" and float(wind) > 7:
    point = 0
  # if maxmum temperature of the day is higher than 35 degree, the point is 0.
  elif tempmax and float(tempmax) > 34:
    point =0
  # if minimum temperature of the day is lower than 3 degree, the point is 0.
  elif tempmin and float(tempmin) < 3:
    point = 0

  # output date and point
  print "%s\t%s" % (date, point)
