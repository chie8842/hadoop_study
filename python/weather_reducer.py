#!/usr/bin/env python

import sys

(last_key, sum_val, count) = (None, 0, 0)
for line in sys.stdin:
  (key, val) = line.strip().split("\t")

  # if key value is not same as previous key value, print previous key and values.
  if last_key and last_key != key:
    print "%s\t%s\t%s\t%s" % (last_key, sum_val, count, float(sum_val)/float(count))
    (last_key, sum_val, count) = (key, int(val), 1)
  # if last key is same as previous key, sum_val = sum_val+ val, and count = count + 1
  else:
    (last_key, sum_val, count) = (key, sum_val + int(val), count + 1)
if last_key:
  # print mmdd,sum of points,sum of information, provability of good weather. 
  print "%s\t%s\t%s\t%s" % (last_key, sum_val, count, float(sum_val) / float(count))
