import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class Mecab {

  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      System.err.println("Usage: Mecab <input path> <output path>");
      System.exit(-1);
    }
   
   Job job = new Job();
   job.setJarByClass(Mecab.class);
   job.setJobName("Mecab");

   FileInputFormat.addInputPath(job, args[0]);
   FileOutputFormat.setOutputPath(job, new Path(args[1]));


   job.setMapperClass(MecabMapper.class);
   job.setReducerClass(MecabReducer.class);

   job.setOutputKeyClass(Text.class);
   job.setOutputValueClass(InputWritable.class);

   System.exit(job.waitForCompletion(true) ? 0 : 1);



 
  }
}
