import java.io.*;

import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.apache.hadoop.mapreduce.Partitioner;


@InterfaceAudience.Public
@InterfaceStability.Stable
public class MyPartitioner<K, V> extends Partitioner<K, V> {
   public int getPartition(K key, V value, int numReduceTasks) {

/*    if (numReduceTasks == 3) {
       boolean b1 = Pattern.matches(key.substring(0,1),"[a-zA-Z]");
       boolean b2 = Pattern.matches(key.substring(0,1),"[^ -~｡-ﾟ]");
       if (b1 == true) {
         return 0;
       } else if (b2 == true) {
         return 1;
       } else{
         return 2;
       }
    } else {
       return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
    }
*/
    int result = -1;
    int code =  (key.hashCode() & Integer.MAX_VALUE) % 10;

//   System.out.println(numReduceTasks + "," + code);
     if (numReduceTasks == 3) {
//    switch (numReduceTasks){
//     case 3:
//      System.out.println("case3");
      if ( code > 1 ){
        result = 2;
      }else{
        result = code;
      }
//     case 2:
    }else if (numReduceTasks == 2) {
//      System.out.println("case2");
      if ( code > 0 ){
        result = 1;
      }else{
        result = 0;
      }
    } else {
     
//     case 1:
//      System.out.println("case1");
      result = code;
   }
//   System.out.println("ReduceTask:" + numReduceTasks);
//   System.out.println(result);
   return result;
  }
}
