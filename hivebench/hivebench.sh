#!/bin/bash

##########################################################
# hivebench.sh
##########################################################
# Copyright (C) 2016 NTT DATA CORPORATION
#
# Usage
# * Run hiveql given time with -t and measure the response time.
#  Example
#   $ bash hivebench.sh -t 10
# * Run hiveql while given time with -T and measure the throughput.
#  Example
#   $ bash hivebench.sh -T 30
# Please confirm more information about options with -h option.
#
# Update Information
# Created on 2016/3/16
# Last Modified on 2016/3/16
##########################################################

CurDate=`date +%Y%m%d_%H-%M-%s`
LogDir=log
RunDir=run
LogDetail=$LogDir/hivebench_detail_`date +%Y%m%d_%H-%M-%S`.csv
LogFile=$LogDir/hivebench_`date +%Y%m%d_%H-%M-%S`.csv

# Function to Exit with Wrong Options
Usage_exit() {
  echo "Usage: $0 [OPTIONS]" 1>&2
  echo "benchmarking options:" 1>&2
  echo "   -c  number of concurrent database clients (default: 1)" 1>&2
  echo "   -t  number of transactions each client runs" 1>&2
  echo "   -T  Query Execution time" 1>&2
  echo "common options:" 1>&2
  echo "   -H hostname" 1>&2
  echo "   -p port" 1>&2
  echo "   -i list of hostname and port" 1>&2
  echo "   -x  hive configuration" 1>&2
  echo "   -e  query" 1>&2
  echo "   -f  query file or directory" 1>&2
  echo "   -h  show usage" 1>&2

  exit 1
}

CNum=1
TNum=-1
CNew=false
MyPS=$$

mkdir -p $LogDir
mkdir -p $RunDir

# オプションのパラメータを取得

#GetOpts() {
while getopts :c:t:T:h:p:i:x:e:f:h opt; do
    case $opt in
# コネクション数の設定（デフォルトは1）
        c)  CNum="$OPTARG"
            ;;
# トランザクションごとに新しく接続する(デフォルトはfalse)
#        C)  CNew="$OPTARG"
#            ;;
# 1コネクションごとのトランザクション数の設定（デフォルトは1、-tと-Tは排他）
        t)  TNum="$OPTARG"
            ;;
# クエリを実行し続ける時間(-tと-Tは排他)
        T)  TTime="$OPTARG"
            ;;
# ホスト名の設定
        H)  HName="$OPTARG"
            ;;
# ポート番号の設定
        p)  Port="$OPTARG"
            ;;
# ホスト名、ポート番号のリスト（-h、-pと排他）
        i)  HList="$OPTARG"
            ;;
# ユーザ、パスワード、hiveconf等の設定ファイル
        x)  Conf="$OPTARG"
            ;;
# クエリの設定(-eと-fは排他)
        e)  Query="$OPTARG"
            ;;
# クエリをファイルで複数指定(トランザクションごとにラウンドロビン、-eと-fは排他)
        f)  QFile="$OPTARG"
            ;;
# ヘルプの表示
        h)  Usage_exit
            ;;
# オプションが足りなかったらヘルプを表示
        *)  Usage_exit
            ;;
    esac
done
#}

# オプションのチェック
CheckOpts() {
# クエリ(-e)とクエリファイル(-f)の両方がない場合は、exit1する。
	if [ -z "$Query" ] && [ -z $QFile ]; then
		echo "option -e or -f is needed."
        	exit 1
	fi
# クエリ(-e)とクエリファイル(-f)の両方ともある場合は、exit1する。
	if [[ -n $Query ]] && [[ -n $QFile ]] ;then
		echo "option -e and -f is exclusive"
		exit 1
	fi
# TNumが初期値<(-tがnull)かつスループット計測時間がない(-Tがnull)の場合は、exit1する。
	if [[ $TNum = -1 ]] && [[ -z $TTime ]] ;then
		echo "TNum=$TNum"
		echo "TTime=$TTime"
		echo "option -t or -T is needed"
		exit 1
	fi
# TNumが初期値以外(-tがnullでない)かつスループット計測時間がある（-Tがnullでない）場合は、exit1する。
	if [[ $TNum != -1 ]] && [[ -n $TTime ]] ;then
		echo "TNum=$TNum"
		echo "TTime=$TTime"
		echo "option -t and -T is exclusive"
		exit 1
	fi
# クエリファイルがディレクトリでもファイルでもない場合は、exit1する。
	if [ ! -n "$Query" ] && [ ! -d $QFile ] && [ ! -f $QFile ]; then
		echo "option -f should be an existed file or directory."
		exit 1
	fi
# -Hもしくは-pの値があり、かつ-iの値がある場合は、exit1する。
	if  [[ -n $HName ]] || [[ -n $Port ]]  && [[ -n $HList ]]; then
		echo "option -h, -p and -i is exclusive"
		exit 1
	fi
# HiveServer2のホスト名(-H)は指定されているが、ポート(-p)は指定がない場合、ポート番号を10000とする。
	if [[ -n $HName ]] && [[ -z $Port ]]; then
		Port=10000
# HiveServer2のポート(-p)は指定されているが、ホスト名(-H)は指定されていない場合、ホスト名をlocalhostとする。
	elif [[ -z $HName ]] && [[ -n $Port ]]; then
		HName=localhost
	fi
# 接続先HiveServer2のリスト(-i)がファイルでない場合はexit1する。
	if [[ -n $HList ]] && [[ ! -f $HList ]]; then
		echo "$HList does not exist."
		exit 1
	fi

}

# 前回の中間ファイルを削除、stty設定の保存
Initialize(){
	rm -f hivebench.hql
	rm -f run/*
	rm -f log/CONNECTION*.log
	rm -f finishfile
	stty -g < /dev/tty > ttyconfig.txt
}

# トランザクション回数分のHiveQLを生成
CreateHQL() {
	QFile=hivebench.hql
	rm -f hivebench.hql

	echo "$Query" >> hivebench.hql
}

# ホスト名、ポートの値から、Hiveserver接続先リストを作成
CreateHList(){
	HList="hlist.txt"
	if [[ -f $HList ]]; then
		echo "hlist.txt is already exist."
		exit 1
	else
		echo "${HName}:${Port}" > $HList
	fi
}

# コネクション数分のhive_child.shを実行 
ExecuteHQL() { 
	echo "Number of Connections is $CNum"
	for ((i=0;i<$CNum;i++)) ; do
		Name="CONNECTION${i}"
		PSTime[$i]=`date +%s`
#		sleep 300 &
		./hivebench_child.sh $i $HList $TNum $QFile $Conf 2>&1 &
		pid=$!
		PETime[$i]=`date +%s`
		echo $pid > "$RunDir/$Name.pid"
	done
}

# スループット計測時間の間hive_child.shを実行
ExecuteHQLWhileTTime(){
	TNum=-1	
	ExecuteHQL

# スループット計測時間が過ぎるまでsleepする
        echo "sleep $TTime"
        sleep ${TTime}

# sleepが終了したら、hivebench_child.shおよびその配下のbeelineプロセスをkillする
#	kill -INT -$$
	touch finishfile
#        for ((i=0;i<${CNum};i++)); do
#                PID[$i]=`cat "$RunDir/CONNECTION${i}.pid"`
#		killpstree ${PID[$i]}
#	done

# hivebench_child.shはSIGTERMを受け取った後、handlerによりwhileループを抜けて次の処理を行うため、
# その処理が終了するまで待つ。
	ps=1
	while [ $ps -ne 0 ]; do
		ps=`ls -1 $RunDir/ | wc -l`
		echo "$ps child processes are remaining. sleep 1"
		sleep 1
	done
	rm -f finishfile
	echo "Killed all connection."
}


# 全てのトランザクションが実行完了するまで待機
WaitHQL() {
	while true; do
		PNum=`ls -1 $RunDir/ |wc -l`
		if [ $PNum == 0 ]; then
			echo "break"   
			break
		fi
		echo "$PNum child processes are still running. sleep 5 seconds"
		sleep 5
	done
}


# 結果をログと標準出力に出力する。
OutputResult() {
        echo "CNum,TNum,Hiveserver,Query,ExitCode,Time(ms)">>$LogDetail

# コネクションごとのログの内容をひとつのログファイルに集約する。
        for ((j=0;j<$CNum;j++)) ; do
                Name="CONNECTION${j}"
		ConnLog=$LogDir/CONNECTION${j}_Result.log
		if [[ -f $ConnLog ]] ;then
			cat $LogDir/CONNECTION${j}_Result.log >> $LogDetail
		fi
        done
# 実行クエリと成功クエリ(ExitCodeが0のクエリ)の数を数える
	ECode=0
	LNum=0
	while read LLine; do
		if [[ $LNum > 0 ]] ;then
			LCode=`echo $LLine | cut -d  "," -f 5`
			ECode=`expr $ECode + $LCode`
			LNum=`expr $LNum + 1`
		fi
	done < $LogDetail
	Trialtemp=`cat $LogDetail | wc -l`

	Trial=`expr $Trialtemp - 1`
	Success=`expr $Trial - $ECode`


	echo "Trial:`expr $Trial`"
	echo "Success:$Success"

}

# 結果を標準出力する
StdOut(){

	echo "------------------------------------"
	echo "          Measured Values           "
	echo "------------------------------------"
	column -s "," -t $LogDetail
	echo ""
	echo "------------------------------------"
	echo "         Summarized Report          "
	echo "------------------------------------"
	column -s "," -t $LogFile
}

# HiveserverおよびqueryごとにAverage、Max、Minを求める
CalcResult() {
	HSum=`cat $HList | wc -l`
	QSum=`ls -1 $QFile | wc -l`

	# 実行結果（hivebench_detail_*.csv）の内容を1行ずつ読み込む
	while read RLine ;do
		if [ $RLine == "CNum,TNum,Hiveserver,Query,ExitCode,Time(ms)" ]; then
			continue
		fi

		RServer=`echo $RLine | cut -d "," -f 3`
		RQuery=`echo $RLine | cut -d "," -f 4`
		RECode=`echo $RLine | cut -d "," -f 5`
		RTime=`echo $RLine | cut -d "," -f 6`
		# サーバ番号
		RHNum=`cat $HList | grep -n $RServer | cut -d ":" -f 1`
		# クエリ番号
		RQNum=`ls -1 $QFile | grep -n $RQuery | cut -d ":" -f 1`

		# HiveServer2、クエリごとに実行クエリ数、成功クエリ数、成功クエリの総実行時間、成功クエリの実行時間の最大値、成功クエリの実行時間の最小値を集計する。
		RHNumPrev=`expr $RHNum - 1`
		Num=`expr $QSum \* $RHNumPrev + $RQNum`
		# 配列に値がなければ初期化する。
		if [ -z ${RNum[$Num]} ]; then
			RNum[$Num]=0
			RSNum[$Num]=0
			RSumTime[$Num]=0
			RMax[$Num]=0
			RMin[$Num]=9999999999
		fi

		RNum[$Num]=`expr ${RNum[$Num]} + 1`	
		if [ $RECode == 0 ]; then
			RSNum[$Num]=`expr ${RSNum[$Num]} + 1`
			RSumTime[$Num]=`expr ${RSumTime[$Num]} + $RTime`
			if [ $RTime -gt ${RMax[$Num]} ]; then
				RMax[$Num]=$RTime
			fi
			if [ $RTime -lt ${RMin[$Num]} ]; then
				RMin[$Num]=$RTime
			fi
		fi
	done < $LogDetail

	echo "Server,QueryFile,Trial,Success,Throughput(hql/sec),Average(msec),Max(msec),Min(msec)" > $LogFile

# HiveServer、クエリごとにスループット、レスポンスタイムの平均値、最大値、最小値を出力する。
	for ((i=0;i<$HSum;i++)); do
		ii=`expr $i + 1`
		for ((j=0;j<$QSum;j++)); do
			jj=`expr $j + 1`
			Num=`expr $QSum \* $i + $jj`
			Server=`cat $HList | sed -n ${ii}p`
			Query=`ls -1 $QFile | sed -n ${jj}p`
			if [[ -n ${RSumTime[$Num]} ]] && [[ ${RSumTime[$Num]} != 0 ]]; then
				if [[ -n $TTime ]];then
					Throuput[$Num]=`echo "scale=5; ${RSNum[$Num]} / ( ${RSumTime[$Num]} / 1000 ) " | bc | awk '{printf "%f", $0}'`
				else
					Throuput[$Num]="None"
				fi
				Average[$Num]=`echo "scale=5; ${RSumTime[$Num]} / ${RSNum[$Num]}" | bc | awk '{printf "%f", $0}'`
			else
				Throuput[$Num]="None"
				Average[$Num]="None"
				RMax[$Num]="None"
				RMin[$Num]="None"
			fi
			echo "$Server,$Query,${RNum[$Num]},${RSNum[$Num]},${Throuput[$Num]},${Average[$Num]},${RMax[$Num]},${RMin[$Num]}" >> $LogFile
		done
	done
}


# SIGINTが送られたときは、子シェルを全てkillしてから自分自身も終了する。
killpstree(){
	local children=`ps --ppid $1 --no-heading | awk '{ print $1 }'`
	for child in $children ; do
        	killpstree $child
    	done
	kill $1
}

# 中間ファイルの削除、tty設定のリストア
CleanUp() {

	ttyconfig=`cat ttyconfig.txt`
	stty $ttyconfig < /dev/tty
	rm -f hlist.txt
	rm -f ttyconfig.txt
	rm -f $RunDir/*.pid
}


# SIGINTを受け取った際のハンドラ
handler()
{
	killpstree $$
	CleanUp
	exit 1
}

##############################################
#Main Function                              
##############################################

#Delete old logfiles
Initialize
#GetOpts
CheckOpts

# -eでクエリが直接指定されている場合は、クエリをファイルに記述する。
if [[ -n "$Query" ]]; then
	CreateHQL
fi

# -H、-pでHiveServer2が指定されている場合は、設定内容をHiveServer2リストに記述する。
if [[ -z $HList ]]; then
	CreateHList
fi

# SIGINT（Ctrl+C等）のシグナルを受け取った場合は、子プロセスを全てkillして異常終了する。
trap handler SIGINT

HSum=`cat $HList | wc -l`
QSum=`ls -1 $QFile | wc -l`
:
echo "Number of hiveserver2 is $HSum"
echo "Number of Query is $QSum"

# スループットを計測する。
if [[ -n $TTime ]]; then
	echo "Start measuring throughput of hiveserver2"
	ExecuteHQLWhileTTime

# レスポンスタイムを計測する。
else
	echo "Start measuring response time of hiveserver2"
	ExecuteHQL
	WaitHQL
fi

# 結果を出力する。
OutputResult
CalcResult
StdOut

# スクリプトを正常終了する。
CleanUp
exit 0

