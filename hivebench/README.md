# How to use hivebench　tool

## How to install
To use this tool, please make work directory and put following scripts under that.  
    1.hivebench.sh  
    2.hivebench_child.sh  

## How to use
usage explession is below.  

```
$ ./hivebench.sh -h  
Usage: ./hivebench.sh [OPTION]  
benchmarking options:  
   -c  number of concurrent database clients (default: 1)  
   -t  number of transactions each client runs (default: 10)  
   -T  Query Execution time  
   -x  hive configuration  
   -e  query  
   -f  query file  
common options:  
   -H hostname  
   -p port  
   -i list of hostname and port 
   -u user  
   -w password  
   -d driver name  
   -h  show usage  
```
   
   * -t and -T are exclusive.  
   * -e and -f are exclusive.  
   * -f can be set a file or a directory. If a directory was set at the option, hql files at the directory was used in a round robin.  
   * (-H, -p) and -i are exclusive.  
   * -x can be set a file or a directory. If a directory was at the option, the conf file which has same name as hqlfile and  extention is .sh is used when beeline command executed.  
  
## Usage Example
Following is an Usage Example  
```
$ ls hqldir  
test1.hql test2.hql test3.hql  
$ cat test1.hql  
select * from test;  
select * from test2;  
$ ls confdir  
  test1.sh test2.sh test3.sh  
$ cat test1.sh  
#!/bin/sh  
USER=hive  
PASSWORD=hive  
INITIALIZE="a=b"  
HIVECONF="c=d"  
HIVEVAR="e=f"  
$ cat serverlist.txt  
192.168.0.1:10000  
192.168.0.2:10000  
$ .bash hivebench.sh -i serverlist.txt -c 5 -f hqldir -T 300 -x confdir  
```
Result format is below.  
```
 ------------------------------------  
          Measured Values            
 ------------------------------------  
CNum  TNum  Hiveserver         Query      ExitCode  Time(ms)  
0     0     192.168.0.1:10000  test.hql   0         4558  
0     1     localhost:10000    test2.hql  0         50113  
0     2     master1:10000      test3.hql  0         41663  
0     3     127.0.0.1:10000    test.hql   0         1878  
0     4     192.168.0.1:10000  test2.hql  0         40040  
0     5     localhost:10000    test3.hql  0         39605  
0     6     master1:10000      test.hql   0         1537  
0     7     127.0.0.1:10000    test2.hql  0         42140  
0     8     192.168.0.1:10000  test3.hql  0         41604  
0     9     localhost:10000    test.hql   0         1380  
1     0     localhost:10000    test2.hql  0         48213  
1     1     master1:10000      test3.hql  0         43671  
1     2     127.0.0.1:10000    test.hql   0         1676  
1     3     192.168.0.1:10000  test2.hql  0         47663  
1     4     localhost:10000    test3.hql  0         43823  
[...]  

------------------------------------  
         Summarized Report          
------------------------------------  
Server             QueryFile  Trial  Success  Throughput(hql/sec)  Average(msec)  Max(msec)  Min(msec)  
192.168.0.1:10000  test.hql   3      3        0.010000             2594.000000    4558       1447  
192.168.0.1:10000  test2.hql  5      5        0.016660             46820.000000   56659      40040  
192.168.0.1:10000  test3.hql  5      5        0.016660             43458.000000   48052      39975  
localhost:10000    test.hql   5      5        0.016660             1432.600000    1470       1380  
localhost:10000    test2.hql  2      2        0.006660             49163.000000   50113      48213  
localhost:10000    test3.hql  5      5        0.016660             42121.000000   47828      39605  
master1:10000      test.hql   5      5        0.016660             1599.000000    1832       1379  
master1:10000      test2.hql  3      3        0.010000             41631.000000   43570      39684  
master1:10000      test3.hql  3      3        0.010000             41923.666660   43671      40437  
127.0.0.1:10000    test.hql   4      4        0.013330             2256.250000    4046       1425  
127.0.0.1:10000    test2.hql  5      5        0.016660             43474.000000   45587      41987  
127.0.0.1:10000    test3.hql  2      2        0.006660             38622.000000   39636      37608  
```

## Output Format
You can see same result as above stdout with delimited csv formated file.  
They are outputed at log/ directory.  
1.hivebench_detail_yyyymmdd_hh-mm-ss.csv # Measured Values  
2.hivebench_yyyymmdd_hh-mm-ss.csv        # Summarized Report  



