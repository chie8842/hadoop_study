#!/bin/bash


#create table and load data
 
CTABLE="CREATE TABLE hivebench (name STRING, num int) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';"
LDATA="LOAD DATA LOCAL INPATH './testdata' OVERWRITE INTO TABLE hivebench;"


beeline -u jdbc:hive2:// -n hive -p hive -e $CTABLE
beeline -u jdbc:hive2:// -n hive -p hive -e $LDATA
