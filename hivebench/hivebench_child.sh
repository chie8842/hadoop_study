#!/bin/bash

##########################################################
# hivebench_child.sh
##########################################################
# Copyright (C) 2016 NTT DATA CORPORATION
#
# Usage
# This script should be called from hivebench.sh.
# Created on 2016/3/16
# Last Modified on 2016/3/16
##########################################################

LogDir=log
RunDir=run
CNum=$1
HList=$2
TNum=$3
QFile=$4
Conf=$5
Name=CONNECTION${CNum}
echo "CONNECTION${CNum} started"

# 設定ファイルの内容を取り込む
Configuration() {
	if [[ -d $Conf ]]; then
		QFileX2=${QFileX%.*}.sh
		ConfShell=$Conf/${QFileX2##*/}
		. $ConfShell
	elif [[ -f $Conf ]]; then
		ConfShell=$Conf
		. $ConfShell
	fi
}

# 設定ファイルから読み込んだConfigurationからコマンドを作る
CreateCommand() {
	COMMAND="beeline -u jdbc:hive2://${HInfo}"
	if [[ -n $User ]]; then
		COMMAND="$COMMAND -n $User"
	fi
	if [[ -n $PASSWORD ]]; then
		COMMAND="$COMMAND -p $PASSWORD"
	fi
	if [[ -n $DRIVER ]]; then
		COMMAND="$COMMAND -d $DRIVER"
	fi
	if [[ -n $INITIALIZE ]]; then
		COMMAND="$COMMAND -i $INITIALIZE"
	fi
	if [[ -n $HIVECONF ]]; then
		IFSBackup=$IFS
		IFS=","
		HIVECONFARR=($HIVECONF)
		IFS=$IFSBackup
		for v in ${HIVECONFARR[@]}; do
			COMMAND="$COMMAND --hiveconf $v"
		done
	fi
	if [[ -n $HIVEVAR ]]; then
		IFSBackup=$IFS
		IFS=","
		HIVEVARARR=($HIVEVAR)
		IFS=$IFSBackup		
		for v in ${HIVEVARARR[@]}; do
			COMMAND="$COMMAND --hivevar $v"
		done
	fi
}

HNum=`cat $HList | wc -l`
# クエリを無限ループで発行（ラウンドロビン）
if [[ $TNum -eq "-1" ]] ;then
        i=`expr 0 + $CNum`
# QFileがディレクトリの場合、そのディレクトリ内のHQLファイルをラウンドロビンで実行する。
# SIGTERMを受け取ったら、ループを抜ける。
        QNum=`ls -1 $QFile|wc -l`
	flag="TRUE"
        while [[ $flag -eq "TRUE" ]]; do
                QREM=`expr $i % $QNum + 1`
                QFileX=`ls -1 $QFile|sed -n ${QREM}p`
                HREM=`expr $i % $HNum + 1`
                HInfo=`cat $HList | sed -n ${HREM}p`

# 設定ファイルを読み込んで、beelineコマンドを作成する。
                Configuration
                CreateCommand

# beelineを実行する。
		STime[$i]=$(printf '%ld' $(expr `date +%s%N` / 1000000))
                if [[ -d $QFile ]] ; then
			echo "CONNECTION$CNum $COMMAND -f ${QFile}/${QFileX}"
			$COMMAND -f ${QFile}/${QFileX} >> ${LogDir}/${Name}.log 2>&1
		else
			echo "CONNECTION$CNum $COMMAND -f ${QFileX}"
			$COMMAND -f ${QFileX} >> ${LogDir}/${Name}.log 2>&1
		fi

                Result[$i]=`echo $?`
		ETime[$i]=$(printf '%ld' $(expr `date +%s%N` / 1000000))

# hivebench.shによってfinishfileが置かれていたら、whileループを抜ける。
		if [[ -f finishfile ]] ;then
			break
		fi
                i=`expr $i + 1`
        done


# スループット計測の終了後、コネクション番号ごとに結果をログファイルに記述する。
	jnum=`expr 0 + $CNum`
	for ((j=jnum;j<$i;j++)) ; do
		Time=$((ETime[$j] - STime[$j]))
		QREM=`expr $j % $QNum + 1`
		QFileX=`ls -1 $QFile|sed -n ${QREM}p`
		HREM=`expr $j % $HNum + 1`
		HInfo=`cat $HList | sed -n ${HREM}p`
		echo "$CNum,`expr $j - $CNum`,$HInfo,${QFileX},${Result[$j]},${Time}" >> "${LogDir}/${Name}_Result.log"
	done
	
# トランザクション数(TNum)の分クエリをループする
else
# QFileがディレクトリの場合、そのディレクトリ内のHQLファイルをラウンドロビンで実行する。
# コネクションを毎回張り直す
	QNum=`ls -1 $QFile|wc -l`
	for ((i=0;i<$TNum;i++)); do
                trap handler SIGTERM
                QREM=`expr $i % $QNum + 1`
                QFileX="`ls -1 $QFile|sed -n ${QREM}p`"
                HREM=`expr $i % $HNum + 1`
                HInfo="`cat $HList | sed -n ${HREM}p`"

# 設定ファイルを読み込んで、beelineコマンドを作成する。
		Configuration
		CreateCommand

# beelineを実行する。
		STime[$i]=$(printf '%ld' $(expr `date +%s%N` / 1000000))

                if [[ -d $QFile ]] ; then
			echo "$COMMAND -f ${QFile}/${QFileX}"
			$COMMAND -f ${QFile}/${QFileX} >> ${LogDir}/${Name}.log 2>&1
		else
			echo "$COMMAND -f ${QFileX}"
			$COMMAND -f ${QFileX} >> ${LogDir}/${Name}.log 2>&1
                fi

                Result[$i]=`echo $?`
		ETime[$i]=$(printf '%ld' $(expr `date +%s%N` / 1000000))
        done

# 全てのトランザクション完了後、コネクション番号ごとに結果をログファイルに記述する。
        for ((j=0;j<$i;j++)) ; do
                Time=$((ETime[$j] - STime[$j]))
                QREM=`expr $j % $QNum + 1`
                QFileX=`ls -1 $QFile|sed -n ${QREM}p`
                HREM=`expr $j % $HNum + 1`
                HInfo=`cat $HList | sed -n ${HREM}p`
                echo "$CNum,$j,$HInfo,${QFileX},${Result[$j]},${Time}" >> "${LogDir}/${Name}_Result.log"
        done
fi
	rm -f "$RunDir/$Name.pid"
	exit 0

echo "CONNECTION${CNum} finished"
