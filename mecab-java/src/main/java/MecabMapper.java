import java.io.File;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapred.*;

import org.chasen.mecab.MeCab;
import org.chasen.mecab.Tagger;
import org.chasen.mecab.Model;
import org.chasen.mecab.Lattice;
import org.chasen.mecab.Node;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;


public class MecabMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  private final static IntWritable one = new IntWritable(1);
 
//  public void map(LongWritable key, Text value,
//      OutputCollector<Text, IntWritable> output, Reporter reporter)

    public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {
//       System.out.println("java.library.path=");
//       String orgprop = System.getProperty("java.library.path");
//       System.out.println("orgprop=");
//       System.out.println(orgprop);
 
//       String newprop = orgprop + ":/usr/local/lib";
//       System.out.println("newprop=");
//       System.out.println(newprop);
    //   System.setProperty("java.library.path", "/usr/local/lib");
  //     System.out.println(System.getProperty("java.library.path"));
//       System.out.println("\n");

//     try {
//       File f = new File("/usr/local/lib/libMeCab.so");
//       System.load(f.toString());
       System.load("/usr/local/lib/libMeCab.so");
//       System.loadLibrary("MeCab");
//      } catch (UnsatisfiedLinkError e) {
//         System.err.println("Cannot load the example native code.\nMake sure your LD_LIBRARY_PATH contains \'.\'\n" + e);
//        System.exit(1);
//      }
     
      Tagger tagger = new Tagger();
      String line = value.toString();
      System.out.println("0" + line);
      System.out.println("1" + tagger.parse(line));
      String taggerline = tagger.parse(line);
      //Node node = tagger.parseToNode(str);
      Node node = tagger.parseToNode(line);

      byte[] asciiCodes;
      try {
        asciiCodes = taggerline.getBytes("US-ASCII");
      } catch (Exception e) {
	e.printStackTrace();
        return;
      }
//      for (int i = 0; i < asciiCodes.length; i++) {
//	System.out.println("asciiCodes[" + i + "]" + asciiCodes[i]);     
//      }

      

      String taggerArr[] = taggerline.split("\n");
      System.out.println("length=" + taggerArr.length);
      for  (int i = 0; i < taggerArr.length; i++) {
//        System.out.println("taggerarray[" + i + "]=" + taggerArr[i]);
        String surface[]  = taggerArr[i].split("\t");
        for (int j = 0; j < surface.length; j++) {
           System.out.println("surface[" + j + "]=" + surface[j]);
           if (surface.length != 2) {
              continue;
           }
           String featureArr[] = surface[1].split(",");
           System.out.println("4" + "word:" + surface[0] + " 品詞：" +  featureArr[0]);     
           if ( featureArr[0].equals("名詞")||featureArr[0].equals("動詞")||featureArr[0].equals("形容詞")){
              System.out.println("5" + "word:" + surface[0] + " 品詞：" +  featureArr[0]);
              context.write(new Text(surface[0]), one);
           } 
            

        }
      }


  }
}
      

