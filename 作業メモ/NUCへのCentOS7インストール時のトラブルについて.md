# NUCへのCentOS7インストール時のトラブルについて
##はじめに
お勉強のために、Hadoop環境構築をすることになり、
せっかくだからCentOS7つかってみよう！とおもったけれど、うまく動作してくれなかったので、
内容を記録しておく。

##1. CDHのRequirement
インストールCDはCentOS7.0だったが、CDHの動作環境を確認したところ、最新バージョンのみCentOS7に対応していた。ただし、
以下の記述あり。
```
  Important: Cloudera supports RHEL 7 with the following limitations:
Only RHEL 7.1 is supported. RHEL 7.0 is not supported.
Only a new installation of RHEL 7.1 is supported. Upgrades from RHEL 6 to RHEL 7.1 are not supported. For more information, 
```

[http://www.cloudera.com/downloads/cdh/5-5-1.html]

なので、インストールしてから7.1にアップデートする方法をとることにした。

##2. CentOS7.1は、配布終了していた
CentOS7.0インストール後、yumリポジトリでバージョンを7.1.1503に固定してyum updateを実行してみた。
すると、アップデート完了後、
```sh
# cat /etc/redhat-release
CentOS Linux release 7.2.1511 (Core)
# uname -r
3.10.0-123.el7.x86_64
```

・・・ん？
7.2になってる。。。
おかしいと思い、CentOS7.1のダウンロードページに行ってみると、

```
This directory (and version of CentOS) is deprecated.
```

CentOS7.1は廃止されていました。。。
一応以下から入手はできるけど。
[http://vault.centos.org/]


なので、CentOS7.2のまま続けることにしました。

## 3. なぞのパケットがフラッディングする。
作業中に、いきなりNW上のすべての端末にNW接続できなくなった。
tcpdumpを取得してみると、なんかなぞのパケットが大量に流れていた。
```sh
# tcpdump
MPCP Opcode Pause and unresponsive computer
MPCP Opcode Pause and unresponsive computer
MPCP Opcode Pause and unresponsive computer
・・・
```
debianとかだとBug報告されている。
[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=709616]

OS再起動してもまたしばらくすると事象が発生する。
/var/log/messagesなど見てみたが、特にエラーメッセージはなし。
よくわからないのでOS再インストール。

## ファイアウォールの設定
yumリポジトリをたてたので、httpdを起動したが、アクセスできない。
80番ポートListenしているし、iptablesもOFFにしているのになんでかと思ったら、
CentOS7では、firewalldとかいう新機能ができてた。
これをOFFにすると、アクセスできるようになった。

## yum.noarchがupdateできない
yum --downloadonlyでダウンロードして作成したローカルリポジトリで他サーバのyum updateを実行しようとしたところ、yum.noarchが更新できなかった。

```
Transaction check error:
  file /etc/yum/yum-cron-hourly.conf from install of yum-3.4.3-132.el7.centos.0.1.noarch conflicts with file from package yum-cron-3.4.3-125.el7.centos.noarch
  file /etc/yum/yum-cron.conf from install of yum-3.4.3-132.el7.centos.0.1.noarch conflicts with file from package yum-cron-3.4.3-125.el7.centos.noarch
```

以下のバグに該当すると思われる。

[https://bugzilla.redhat.com/show_bug.cgi?id=1293713:title]

## 4. Mini Displayportを抜き差しすると変な挙動をする。
ディスプレイとの接続に、Mini Displayportを使用していたが、これを抜き差しすると、
以下のような挙動をするときがある。
・ディスプレイ信号がなくなる→Ctrl+Shift+F6を押すと、CUIモードになって返ってくる。
・表示言語が変わる
・CUIモードになる
原因はよくわからないが、再起動すると元に戻った。

## 5. Selinuxの設定をミスったせいで、カーネルクラッシュしてブートできなくなる。
･/etc/sysconfig/selinux上で、スペルミスをして保存した結果、
次の再起動時にカーネルクラッシュして起動できなくなった。
以下のページを参考に、boot configurationをいじると、起動できた。

[https://asafshoval.wordpress.com/2014/11/18/overcome-fail-to-load-selinux-policy-freezing-error-message-while-booting-linux/:title]

## 6. Wifi子機のUSBをひっこぬいたあと、NetworkMangerがあがらない
NetworkManager、networkの再起動を行おうとしたところ、起動しなくなった。
デバッグモードで再起動してみたところ、以下のエラーが出ている。
```sh
# bash -x /etc/init.d/network restart
・・・
インターフェース ens1f1 を活性化中:  エラー: 接続のアクティベーションに失敗: Connection 'ens1f1' is not
available on the device ens1f1 at this time.
```
Wifi子機USBを挿してから実行したところ、再起動できた。
インタフェースを自動起動設定にしていると、つながってないときに再起動エラーするみたいなので、
自動起動をOFFにした。
```
# nmcli connection modify ens1f1 connection.autoconnect no
```

## 7. Xserverがあがらない
abrtレポートを作成せよ、とのメッセージがあったので、作成したところ、
以下のバグに該当した。
[https://bugs.centos.org/view.php?id=8657#c25368:title]


## 8.Xserverがあがらない２
下記のメッセージがでて、GUIがあがらない。
oh no something has gone wrong centos7
→Gnomeのバグ？
[https://bugs.archlinux.org/task/44548:title]

次から次へとトラブルが起こるので、CentOS7はあきらめて、CentOS6をインストールすることにしました。



