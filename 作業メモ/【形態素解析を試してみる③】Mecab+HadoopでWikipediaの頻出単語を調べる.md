# 環境
CentOS6.7  
CDH5.2.1（MRv1）  
mecab-0.996　 

# 内容
## Mapper処理
mecab-test-mapper.py
```python
import sys
import MeCab
import sys
import string

for sentence in sys.stdin:

    t = MeCab.Tagger (" ".join(sys.argv))
    val = t.parse(sentence)
    words = val.split("\n")
    for word in words:
        worda = word.split("\t")
        if len(worda) == 2:
           wordb = worda[1].split(",")
           if wordb[0] == "名詞" or wordb[0] == "動詞" or wordb[0] == "形容詞":
             print "%s\t%s" % (wordb[6],1)
```

## Reducer処理
mecab-test-reducer.py
```python
#!/usr/bin/env python

import sys

(last_key, sum_val) = (None, 0)
for line in sys.stdin:
  (key, val) = line.strip().split("\t")
  if last_key and last_key != key:
    print "%s\t%s" % (last_key, sum_val)
    (last_key, sum_val) = (key, int(val))
  else:
    (last_key, sum_val) = (key, sum_val + int(val))

if last_key:
  print "%s\t%s" % (last_key, sum_val)
```
wikipediaの記事をHDFS上に格納する。
```sh
$ sudo -u hdfs hdfs dfs -mkdir -p /data/wikipedia/input
$ sudo -u hdfs hdfs dfs -put /tmp/extracted/* /data/wikipedia/input/
```

mapreduceを実行する。
```sh
$ hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming.jar \
 -files /tmp/scripts/weather_test_mapper.py,/tmp/scripts/weather_test_reducer.py \
 -input /data/wikipedia/input/*/*/* -output /data/wikipedia/output \
 -mapper /work/script/mecab-test-mapper.py\
 -reducer /work/script/mecab-test-reducer.py
```

最初、mecab-test-reducer.pyの15行目は、
(last_key, sum_val) = (key, sum_val + int(val))じゃなくて   
(last_key, sum_val) = (key, sum(sum_val,int(val))ってかいてたけど、以下のようにおこられた。   
```sh
$ cat sample.txt |python mecab_mapper.py |\sort |python mecab_reducer.py
Traceback (most recent call last):
  File "mecab_reducer.py", line 15, in <module>
    (last_key, sum_val) = (key, sum(sum_val, 1))
TypeError: 'int' object is not iterable
```
sum()が使えなかったのはなんでなんだろう。。。