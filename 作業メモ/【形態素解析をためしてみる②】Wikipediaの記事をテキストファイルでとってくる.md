#環境
・CentOS6.7
・Python2.6.6、2.7.9

#手順
## Wikipediaの記事をダウンロード
```sh
$wget http://dumps.wikimedia.org/jawiki/latest/jawiki-latest-pages-articles.xml.bz2
```
※サイズが大きいので注意。
わたしのダウンロードしたときは、2.1Gで2,3時間かかりました。

## WikiExtracterでダウンロードした記事をテキスト化
以下からWikiExtracterをダウンロード

[http://medialab.di.unipi.it/wiki/Wikipedia_Extractor:title]
下の方にDownloadsの項目がある。
```sh
$curl http://medialab.di.unipi.it/Project/SemaWiki/Tools/WikiExtractor.py > WikiExtractor.py
```

extractedディレクトリを作成し、その中に250Kずつに分割した記事データを格納する。
```sh
$mkdir extracted
$python WikiExtractor.py -cb 250K -o extracted jawiki-latest-pages-articles.xml.bz2
  File "WikiExtractor.py", line 902
    afterPat = { o:re.compile(openPat+'|'+c, re.DOTALL) for o,c in izip(openDelim, closeDelim)}
                                                          ^
SyntaxError: invalid syntax
```
エラーが出るので、WikiExtractor.pyの902行目を以下のように書き換える。
```python
(修正前)  
afterPat = { o:re.compile(openPat+'|'+c, re.DOTALL) for o,c in izip(openDelim, closeDelim)}
(修正後)
afterPat = dict()
for o,c in izip(openDelim, closeDelim):
afterPat[o] = re.compile(openPat + '|' + c, re.DOTALL)
```

再度トライ
```sh
$ python WikiExtractor.py -cb 250K -o extracted jawiki-latest-pages-articles.xml.bz2
INFO: Preprocessing 'jawiki-latest-pages-articles.xml.bz2' to collect template definitions: this may take some time.
Traceback (most recent call last):
  File "WikiExtractor.py", line 2591, in <module>
    main()
  File "WikiExtractor.py", line 2587, in main
    args.compress, args.processes)
  File "WikiExtractor.py", line 2303, in process_dump
    load_templates(input, template_file)
  File "WikiExtractor.py", line 2226, in load_templates
    define_template(title, page)
  File "WikiExtractor.py", line 1394, in define_template
    text = re.sub(r'<noinclude\s*>.*$', '', text, flags=re.DOTALL)
TypeError: sub() got an unexpected keyword argument 'flags'
```
またエラー。
GitHub上の[README](https://github.com/attardi/wikiextractor)を確認したところ、Python2.7にしか対応していないとのこと。
仕方ないのでpythonzでpython2.7を入れて、再度チャレンジ。

## pythonzおよびPython2.7.9インストール
```
$ curl -kL https://raw.github.com/saghul/pythonz/master/pythonz-install | bash
$ /usr/local/pythonz/bin/pythonz version
1.11.0
$ /usr/local/pythonz/bin/pythonz list
# Installed Python versions
```
まだ何も入っていないので、Python2.7.9をインストールする。
```sh
$ /usr/local/pythonz/bin/pythonz install 2.7.9
$ /usr/local/pythonz/bin/pythonz list
# Installed Python versions
  CPython-2.7.9
# /usr/local/pythonz/pythons/CPython-2.7.9/bin/python2.7 --version
Python 2.7.9
```
Python2.7.9がインストールできたので、再度WikiExtractor.pyを実行してみる。
```sh
$ /usr/local/pythonz/pythons/CPython-2.7.9/bin/python2.7 WikiExtractor.py -cb 250K -o extracted jawiki-latest-pages-articles.xml.bz2
Traceback (most recent call last):
  File "WikiExtractor.py", line 55, in <module>
    import bz2
ImportError: No module named bz2
```

bz2-develがない状態でpython2.7をコンパイルすると、こういうエラーになるらしい。
[http://qiita.com/kakkunpakkun/items/ac6cb84e37d09d768b46:title]

```sh
$yum list installed |grep bz2-devel
$yum -y install bz2-devel
```
python2.7.9を再インストール
```sh
$/usr/local/pythonz/bin/pythonz uninstall 2.7.9
$/usr/local/pythonz/bin/pythonz list
$/usr/local/pythonz/bin/pythonz install 2.7.9
```
再度WikiExtractorを実行
```sh
# /usr/local/pythonz/pythons/CPython-2.7.9/bin/python2.7 WikiExtractor.py -cb 250K -o extracted jawiki-latest-pages-articles.xml.bz2
```

やっとできた！
