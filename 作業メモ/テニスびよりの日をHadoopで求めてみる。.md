テニスがすきなので、  
過去の気象データから、テニスびよりな確率が高い日をMapReduceで算出してみた。  
気象データは、以下の気象庁のページからとってきました。  
[http://www.data.jma.go.jp/gmd/risk/obsdl/index.php#:title]

#環境
・CentOS6.7  
・Hadoop：CDH5.2.1  
・Python2.6.6  

#条件
・場所：東京  
・過去データ：1872/1/1～2015/12/31  
1872年て、明治4年です。岩倉使節団がサンフランシスコに到着した年です。  
そんな昔から気象データ残ってることにびっくりしました。笑  
・テニスびよりの条件：  
　1日の降水量が10ミリ以下  
  風の強さが7m以下  
  1日の最高気温が35度以下  
  1日の最低気温が3度以上  

#プログラムの内容
weather_mapper.py
```python
#!/usr/bin/env python

import re
import sys

for line in sys.stdin:
  val = line.split(",")
  date = "/".join([val[1], val[2]])
  rain = val[3]
  tempmax = val[5]
  tempmin = val[6]
  wind = val[7]

  point = 1

  # if summary of rain of the day is more than 10, the point is 0.
  if rain and float(rain) > 10:
    point = 0
  # if average of wind of the day is stronger than 7, the point is 0.
  elif wind and wind != "\n" and float(wind) > 7:
    point = 0
  # if maxmum temperature of the day is higher than 35 degree, the point is 0.
  elif tempmax and float(tempmax) > 34:
    point =0
  # if minimum temperature of the day is lower than 3 degree, the point is 0.
  elif tempmin and float(tempmin) < 3:
    point = 0

  # output date and point
  print "%s\t%s" % (date, point)

```
  
weather_reducer.py
```python
#!/usr/bin/env python

import sys

(last_key, sum_val, count) = (None, 0, 0)
for line in sys.stdin:
  (key, val) = line.strip().split("\t")

  # if key value is not same as previous key value, print previous key and values.
  if last_key and last_key != key:
    print "%s\t%s\t%s\t%s" % (last_key, sum_val, count, float(sum_val)/float(count))
    (last_key, sum_val, count) = (key, int(val), 1)
  # if last key is same as previous key, sum_val = sum_val+ val, and count = count + 1
  else:
    (last_key, sum_val, count) = (key, sum_val + int(val), count + 1)
if last_key:
  # print mmdd, provability of good weather.
  print "%s\t%s\t%s\t%s" % (last_key,float(sum_val) / float(count))
```

実行してみる。
```sh
hadoop jar /usr/lib/hadoop-0.20-mapreduce/contrib/streaming/hadoop-streaming.jar \
   -files weather_test_mapper2.py,weather_test_reducer3.py
   -input /data/weather/input/*.csv 
   -output /data/weather/output 
   -mapper weather_mapper.py 
   -reducer weather_reducer.py
```

終わったら、実行結果をみてみる。
```sh
 hadoop fs -cat /data/weather/output3/p* |sort -k2 -r
11/3    134     144     0.930555555556
7/28    131     144     0.909722222222
5/6     131     144     0.909722222222
5/26    131     144     0.909722222222
10/29   131     144     0.909722222222
4/30    130     144     0.902777777778
11/7    130     144     0.902777777778
5/28    129     144     0.895833333333
5/1     129     144     0.895833333333
11/5    129     144     0.895833333333
5/19    128     144     0.888888888889

```

ついでにグラフにしてみる。
[f:id:love_tennis0708:20160221213850p:plain]
  

冬は寒いから確率低いですね。
最低気温の条件が厳しかったかな。
梅雨の影響はもう少し強いとおもったけど、そーでもなかった。

