# 環境
・CentOS6.7
・Python2.6.6

# インストールするもの
・mecab-0.996
・mecab-ipadic-2.7.0
・mecab-python-0.996

#手順
## mecab-0.996インストール

```sh
$cd /tmp
$curl -O https://mecab.googlecode.com/files/mecab-0.996.tar.gz
$ls mecab-0.996.tar.gz
$tar zxvf mecab-0.996.tar.gz
$cd mecab-0.996
$./configure
$make
$make install
```

##mecab-ipadic-2.7.0インストール
```sh
$cd /tmp
$curl -O https://mecab.googlecode.com/files/mecab-ipadic-2.7.0-20070801.tar.gz
$tar zxfv mecab-ipadic-2.7.0-20070801.tar.gz
$cd mecab-ipadic-2.7.0-20070801/
$./configure --with-charset=utf8
$make
$make install
```

##mecab-python-0.996インストール
```sh
$cd /tmp
$curl -O https://mecab.googlecode.com/files/mecab-python-0.996.tar.gz
$tar zxfv mecab-python-0.996.tar.gz
$cd mecab-python-0.996
```
設定ファイルを編集する
```
$vi setup.py
```
以下のように
書き換える内容：mecab-config　->　/tmp/mecab-0.996/mecab-config
viのコマンド
```
:%s;mecab-config;/tmp/mecab-0.996/mecab-config;g
```
###変更前のファイル
```python
#!/usr/bin/env python

from distutils.core import setup,Extension,os
import string

def cmd1(str):
    return os.popen(str).readlines()[0][:-1]

def cmd2(str):
    return string.split (cmd1(str))

setup(name = "mecab-python",
        version = cmd1("/var/tmp/mecab-config --version"),
        py_modules=["MeCab"],
        ext_modules = [
                Extension("_MeCab",
                        ["MeCab_wrap.cxx",],
                        include_dirs=cmd2("mecab-config --inc-dir"),
                        library_dirs=cmd2("mecab-config --libs-only-L"),
                        libraries=cmd2("/mecab-config --libs-only-l"))
                        ])
```
###変更後のファイル
```python
#!/usr/bin/env python

from distutils.core import setup,Extension,os
import string

def cmd1(str):
    return os.popen(str).readlines()[0][:-1]

def cmd2(str):
    return string.split (cmd1(str))

setup(name = "mecab-python",
        version = cmd1("/tmp/mecab-0.996/mecab-config --version"),
        py_modules=["MeCab"],
        ext_modules = [
                Extension("_MeCab",
                        ["MeCab_wrap.cxx",],
                        include_dirs=cmd2("/tmp/mecab-0.996/mecab-config --inc-dir"),
                        library_dirs=cmd2("/tmp/mecab-0.996/mecab-config --libs-only-L"),
                        libraries=cmd2("/tmp/mecab-0.996/mecab-config --libs-only-l"))
                        ])
```
セットアップ実行
```sh
$python setup.py build
running build
running build_py
creating build
creating build/lib.linux-x86_64-2.6
copying MeCab.py -> build/lib.linux-x86_64-2.6
running build_ext
building '_MeCab' extension
creating build/temp.linux-x86_64-2.6
gcc -pthread -fno-strict-aliasing -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m64 -mtune=generic -D_GNU_SOURCE -fPIC -fwrapv -DNDEBUG -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m64 -mtune=generic -D_GNU_SOURCE -fPIC -fwrapv -fPIC -I/usr/local/include -I/usr/include/python2.6 -c MeCab_wrap.cxx -o build/temp.linux-x86_64-2.6/MeCab_wrap.o
MeCab_wrap.cxx:149:20: error: Python.h: そのようなファイルやディレクトリはありません
MeCab_wrap.cxx:2963:4: error: #error "This python version requires swig to be run with the '-classic' option"
MeCab_wrap.cxx:788: error: ‘PyObject’ was not declared in this scope
MeCab_wrap.cxx:788: error: ‘str’ was not declared in this scope
MeCab_wrap.cxx:789: error: expected ‘,’ or ‘;’ before ‘{’ token
MeCab_wrap.cxx:812: error: expected initializer before ‘*’ token
MeCab_wrap.cxx:838: error: expected initializer before ‘*’ token
MeCab_wrap.cxx:892: error: expected initializer before ‘*’ token
MeCab_wrap.cxx:907: error: ‘inquiry’ does not name a type
MeCab_wrap.cxx:908: error: ‘intargfunc’ does not name a type
MeCab_wrap.cxx:909: error: ‘intintargfunc’ does not name a type
MeCab_wrap.cxx:910: error: ‘intobjargproc’ does not name a type
MeCab_wrap.cxx:911: error: ‘intintobjargproc’ does not name a type
MeCab_wrap.cxx:912: error: ‘getreadbufferproc’ does not name a type
MeCab_wrap.cxx:913: error: ‘getwritebufferproc’ does not name a type
MeCab_wrap.cxx:914: error: ‘getsegcountproc’ does not name a type
MeCab_wrap.cxx:915: error: ‘getcharbufferproc’ does not name a type
MeCab_wrap.cxx:916: error: ‘PyObject’ was not declared in this scope
MeCab_wrap.cxx:916: error: ‘x’ was not declared in this scope
MeCab_wrap.cxx:916: error: expected primary-expression before ‘void’
MeCab_wrap.cxx:916: error: initializer expression list treated as compound expression
MeCab_wrap.cxx:917: error: expected ‘,’ or ‘;’ before ‘{’ token
In file included from /usr/lib/gcc/x86_64-redhat-linux/4.4.7/../../../../include/c++/4.4.7/stdexcept:38,
                 from MeCab_wrap.cxx:2987:
/usr/lib/gcc/x86_64-redhat-linux/4.4.7/../../../../include/c++/4.4.7/exception:35: error: expected declaration before end of line
error: command 'gcc' failed with exit status 1

```
Python.hがないと怒られたので、python-develをインストールしたあと再度セットアップ実行し、インストール
```sh
$sudo yum install python-devel
$python setup.py build
$python setup.py install
```
##ld.so.confの設定
```sh
#libmecab.so.2があることを確認
$ ls /usr/local/lib/libmecab.so.2
/usr/local/lib/libmecab.so.2

$cat /etc/ld.so.conf
include ld.so.conf.d/*.conf

$echo "/usr/local/lib" >> /etc/ld.so.conf
$cat /etc/ld.so.conf
include ld.so.conf.d/*.conf
/usr/local/lib
$sudo ldconfig
```

##mecab-pythonの動作確認
```sh
$ cd /tmp/mecab-python-0.996
$ python test.py
0.996
太郎    名詞,固有名詞,人名,名,*,*,太郎,タロウ,タロー
は      助詞,係助詞,*,*,*,*,は,ハ,ワ
この    連体詞,*,*,*,*,*,この,コノ,コノ
本      名詞,一般,*,*,*,*,本,ホン,ホン
を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
二      名詞,数,*,*,*,*,二,ニ,ニ
郎      名詞,一般,*,*,*,*,郎,ロウ,ロー
を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
見      動詞,自立,*,*,一段,連用形,見る,ミ,ミ
た      助動詞,*,*,*,特殊・タ,基本形,た,タ,タ
女性    名詞,一般,*,*,*,*,女性,ジョセイ,ジョセイ
に      助詞,格助詞,一般,*,*,*,に,ニ,ニ
渡し    動詞,自立,*,*,五段・サ行,連用形,渡す,ワタシ,ワタシ
た      助動詞,*,*,*,特殊・タ,基本形,た,タ,タ
。      記号,句点,*,*,*,*,。,。,。
EOS

        BOS/EOS,*,*,*,*,*,*,*,*
太郎    名詞,固有名詞,人名,名,*,*,太郎,タロウ,タロー
は      助詞,係助詞,*,*,*,*,は,ハ,ワ
この    連体詞,*,*,*,*,*,この,コノ,コノ
本      名詞,一般,*,*,*,*,本,ホン,ホン
を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
二      名詞,数,*,*,*,*,二,ニ,ニ
郎      名詞,一般,*,*,*,*,郎,ロウ,ロー
を      助詞,格助詞,一般,*,*,*,を,ヲ,ヲ
見      動詞,自立,*,*,一段,連用形,見る,ミ,ミ
た      助動詞,*,*,*,特殊・タ,基本形,た,タ,タ
女性    名詞,一般,*,*,*,*,女性,ジョセイ,ジョセイ
に      助詞,格助詞,一般,*,*,*,に,ニ,ニ
渡し    動詞,自立,*,*,五段・サ行,連用形,渡す,ワタシ,ワタシ
た      助動詞,*,*,*,特殊・タ,基本形,た,タ,タ
。      記号,句点,*,*,*,*,。,。,。
        BOS/EOS,*,*,*,*,*,*,*,*
EOS
EOS
filename: /usr/local/lib/mecab/dic/ipadic/sys.dic
charset: utf8
size: 392126
type: 0
lsize: 1316
rsize: 1316
version: 102
``` 

参考サイト：
[http://qiita.com/saicologic/items/ab70e14f7e2ec2ee0b4d:title]

