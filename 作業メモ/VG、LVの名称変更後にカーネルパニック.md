pg-rex実装において、VG、LVの名称をHAクラスタを組む2サーバ上で同じにしたかったため、以下を編集して再起動したところ、カーネルパニックになった。

```sh
# vgs
  VG        #PV #LV #SN Attr   VSize   VFree
  vg_hadoop   1   4   0 wz--n- 222.88g    0
# vgrename vg_master1 vg_hadoop


# vi /etc/fstab
title CentOS (2.6.32-573.18.1.el6.x86_64)
        root (hd0,1)
#（変更前）        kernel /vmlinuz-2.6.32-573.18.1.el6.x86_64 ro root=/dev/mapper/vg_master1-lv_root rd_NO_LUKS rd_LVM_LV=vg_master1/lv_root rd_NO_MD crashkernel=auto  　KEYBOARDTYPE=pc KEYTABLE=jp106 LANG=ja_JP.UTF-8 rd_LVM_LV=vg_master1/lv_swap rd_NO_DM rhgb quiet
　　　　kernel /vmlinuz-2.6.32-573.18.1.el6.x86_64 ro root=/dev/mapper/vg_hadoop-lv_root rd_NO_LUKS rd_LVM_LV=vg_hadoop/lv_root rd_NO_MD crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=jp106 LANG=ja_JP.UTF-8 rd_LVM_LV=vg_hadoop/lv_swap rd_NO_DM rhgb quiet
        initrd /initramfs-2.6.32-573.18.1.el6.x86_64.img
title CentOS 6 (2.6.32-573.el6.x86_64)
        root (hd0,1)
#（変更前）        kernel /vmlinuz-2.6.32-573.el6.x86_64 ro root=/dev/mapper/vg_master1-lv_root rd_NO_LUKS rd_LVM_LV=vg_master1/lv_root rd_NO_MD crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=jp106 LANG=ja_JP.UTF-8 rd_LVM_LV=vg_master1/lv_swap rd_NO_DM rhgb quiet
　　　　kernel /vmlinuz-2.6.32-573.el6.x86_64 ro root=/dev/mapper/vg_hadoop-lv_root rd_NO_LUKS rd_LVM_LV=vg_hadoop/lv_root rd_NO_MD crashkernel=auto  KEYBOARDTYPE=pc KEYTABLE=jp106 LANG=ja_JP.UTF-8 rd_LVM_LV=vg_hadoop/lv_swap rd_NO_DM rhgb quiet
        initrd /initramfs-2.6.32-573.el6.x86_64.img

# vi /boot/efi/grub.conf
・・・(省略)・・・
#(変更前)/dev/mapper/vg_master1-lv_root   /       ext4    defaults        1       1
/dev/mapper/vg_hadoop-lv_root   /       ext4    defaults        1       1
UUID=388dfed4-828b-497d-84aa-6707face842e       /boot   ext4    defaults        1       2
UUID=83B9-D580  /boot/efi       vfat    umask=0077,shortname=winnt      0       0
#(変更前)/dev/mapper/vg_hadoop-LogVol02  /var    ext4    defaults        1       2
/dev/mapper/vg_hadoop-LogVol02  /var    ext4    defaults        1       2
#(変更前)/dev/mapper/vg_hadoop-lv_swap   swap    swap    defaults        0       0
/dev/mapper/vg_hadoop-lv_swap   swap    swap    defaults        0       0
tmpfs   /dev/shm        tmpfs   defaults        0       0
devpts  /dev/pts        devpts  gid=5,mode=620  0       0
sysfs   /sys    sysfs   defaults        0       0
proc    /proc   proc    defaults        0       0
```
原因は、efiでなくBIOSブートなのにefiのgrub.confを編集したので、実際に起動時に使用するgrub.confと/etc/fstabの内容が異なってしまったため。

以下の手順で復旧。
①電源プチしてOS再起動
②カーネルを選ぶ画面で、eボタンを押してLV名称を修正。さらにシングルモードで起動するために末尾に「 s」をつけてEnter
③bボタンでブート
④rootログインする。
⑤「/」のファイルシステムがReadOnlyになっているので、ReadWriteで再マウント
   # mount -n -o remount,rw /
⑥/etc/fstab、/etc/grub.confの設定を修正する。（VG、LVMの名称をあわせる）
⑦再起動