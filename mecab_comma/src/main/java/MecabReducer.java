import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MecabReducer extends Reducer<Text, IntWritable, Text, Text> {

  private IntWritable result = new IntWritable();
  private int sumValue = 0;
  private Text text = new Text();
  @Override
  public void reduce(Text key, Iterable<IntWritable> values, Context context)
      throws IOException, InterruptedException {
    
    sumValue = 0;
    for (IntWritable value : values) {
      sumValue++;
    }
    result.set(sumValue);


//    System.out.println(key + "=" + sumValue);
    text.set("," + result.toString());
    context.write(key, text);
  }
}
