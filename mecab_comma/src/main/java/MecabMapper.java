import java.io.File;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.*;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapred.*;

import org.chasen.mecab.MeCab;
import org.chasen.mecab.Tagger;
//import org.chasen.mecab.Model;
//import org.chasen.mecab.Lattice;
//import org.chasen.mecab.Node;
//import java.util.Iterator;
//import java.util.HashMap;
//import java.util.Map;


public class MecabMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

  private final static IntWritable one = new IntWritable(1);
 
//  public void map(LongWritable key, Text value,
//      OutputCollector<Text, IntWritable> output, Reporter reporter)
//  @Override
//  public void setup(Context context) throws IOException, InterruptedException {
//    System.load("/usr/local/lib/libMeCab.so");
//    Tagger tagger = new Tagger();
//  }

//  Tagger tagger = new Tagger();
  static {
      System.loadLibrary("MeCab");
//       System.load("/usr/local/lib/libMeCab.so");
  }
  private Tagger tagger = new Tagger();



  public void map(LongWritable key, Text value, Context context)
      throws IOException, InterruptedException {

      String line = value.toString();
      String taggerline = tagger.parse(line);

      String taggerArr[] = taggerline.split("\n");
      for  (int i = 0; i < taggerArr.length; i++) {
        String surface[]  = taggerArr[i].split("\t");
        for (int j = 0; j < surface.length; j++) {
           if (surface.length != 2) {
              continue;
           }
           String featureArr[] = surface[1].split(",");
           if ( featureArr[0].equals("名詞")||featureArr[0].equals("動詞")||featureArr[0].equals("形容詞")){
              context.write(new Text(surface[0]), one);
           } 
        }
      }
  }
}
      

